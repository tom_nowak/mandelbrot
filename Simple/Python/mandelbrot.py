import numpy
import cv2
import sys

if(len(sys.argv)!=3):
    print("Usage:", sys.argv[0], " <file with settings> <file with colour pattern>")
    quit()

pattern = numpy.array(
