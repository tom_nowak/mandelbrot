import math

LAST_ITERATION = 999 # LAST_ITERATION+1 is max number of iterations for each pixel - number of colours in the pattern
# colours will change nonlinearly based on iteration:
# red from 255 to 0 - corresponding to 255*cos(0..PI/2)
# green from 0 throug 255 to 0 - corresponding to 255*sin(0..PI/2..PI)
# blue from 0 to 255 - corresponding to 255*sin(0..PI/2)
blue_coeff = red_coeff = math.pi/(2*LAST_ITERATION) # LAST_ITERATIN*red_coeff = M_PI/2
green_coeff = math.pi/LAST_ITERATION # LAST_ITERATIN*green_coeff = M_PI
with open('pattern.txt', 'w') as f:
    for i in range(0, LAST_ITERATION + 1):
        r = int(255*math.cos(i*red_coeff))
        g = int(255*math.sin(i*green_coeff))
        b = int(255*math.sin(i*blue_coeff))
        f.write('{:3d} {:3d} {:3d}\n'.format(b, g, r))

    f.write('0     0   0') 

