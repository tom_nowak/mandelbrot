#include <iostream>
#include <fstream>
#include <complex>
#include <stdexcept>
#include <string>
#include <vector>
#include <array>
#include <limits>
#include <opencv2/highgui/highgui.hpp>

struct Settings
{
    unsigned imageWidth, imageHeight;
    std::complex<double> complexBegin; // point on complex plane corresponding to (0,0) on image 
    double complexRowLength; // length of one row on complex plane corresponding to the whole image width 

    Settings(const char *filename)
    {
        #define READ(x) \
        file >> x; \
        if(!file) \
            throw std::invalid_argument("incorrect contents of settings file (unable to read" #x ")\n"); \
        for(char c = file.get(); c!='\n' && c!=EOF; c = file.get());
        std::string settingsName;
        std::ifstream file(filename);
        if(!file)   
            throw std::invalid_argument("could not open file with settings");
        READ(settingsName)
        if(settingsName != "settings")
            throw std::invalid_argument("file with settings should have signature: settings (did you mess up pattern and settings?)");
        READ(imageWidth)
        READ(imageHeight)
        READ(complexBegin)
        READ(complexRowLength)
        file.close();
        
        #undef READ
    }

    double getComplexStep() // change of complex coordinate corresponding to change of image coordinate by one
    {
        const double result = complexRowLength / (double)imageWidth;
        // perform test for numerical correctness:
        double f1 = complexBegin.real();
        double f2 = f1 + complexRowLength; 
        for(unsigned i=0; i<imageWidth; ++i)
            f1 += result; // check if adding result many times has the same effect as adding complexRowLength once
        f1 -= f2;
        if(fabs(f1) > std::numeric_limits<double>::epsilon())
            std::cerr << "Warning - possible numeric errors while iterating with doubles (for " << imageWidth << " iterations "
                      << fabs(f1/std::numeric_limits<double>::epsilon()) << " times larger than epsilon)\n";
        return result;
    }
};

using Colour = std::array<uint8_t, 3>;

std::istream& operator>> (std::istream &is, Colour &c)
{
    int i;
    for(int j=0; j<3; ++j)
    {
        is >> i;
        if(!is || i<0 || i>255)
        {
            is.setstate(std::ios_base::failbit);
            return is;
        }
        c[j] = i;
    }
    return is;
}

std::ostream& operator<< (std::ostream &os, const Colour &c)
{
    os << +c[0] << " " << +c[1] << " " << +c[2];
    return os;
}

struct Pattern : public std::vector<Colour>
{
    Pattern(const char *filename)
    {
        Colour tmp;
        std::ifstream file(filename);
        if(!file)   
            throw std::invalid_argument("could not open file with pattern");
        for(;;) 
        {
            file >> tmp;
            if(!file)
                break;
            push_back(tmp);
        }
        if(size() < 2)
            throw std::invalid_argument("pattern file should contain at least two lines");
        file.close();
    }
};

// The most important function. It is C-style, because I want rendering algorithm to be independent of OpenCV Mat.
// I also want to be able to call this function in multiple threads. Parameters:
// data - pointer to begin of data. Data is considered to be 8-bit BGR (blue, green, red) array.
// numberOfRows - number of rows (it might not be the same as number of rows in the image - only part of it might be rendered)
// rowLength - number of pixels in one row (as above, only part of each row can be rendered)
// rowStep - number of bytes between end of row number i and begin of row number i+1 (should be 0, unless rendering is performed on image fragment)
// pattern - vector of colours, pixels outside of Mandelbrot's set will be given some colour based on how quickly some series diverges
// complexBegin - point on complex plane corresponding to image begin 
// complexStep - change of complex coordinate corresponding to change of image coordinate by one
void render(Colour *data, const size_t numberOfRows, const size_t rowLength, const size_t rowStep, 
    const Pattern &pattern, const std::complex<double> complexBegin, const double complexStep)
{
    std::complex<double> point = complexBegin;
    for(size_t rowN = 0; rowN < numberOfRows; ++rowN) // for each row
    {
        for(size_t colN = 0; colN < rowLength; ++colN) // for each column in the row
        {
            std::complex<double> z(0,0); // first element of series describing Mandelbrot's set
            size_t i = 0;
            for(i = 0; i < pattern.size(); ++i)
            {
                if(std::norm(z) >= 4) // norm - more computationally efficient version of if(std::abs(z) >= 2)
                    break;
                z = z*z + point;
            }
            *data = pattern[i];
            ++data;
            point += complexStep;
        }
        data += rowStep;
        point = std::complex<double>(complexBegin.real(), point.imag() - complexStep);
    }
}

int main(int argc, char **argv)
{
    if(argc!=4)
    {
        std::cerr << "Usage: " << argv[0] << " <file with settings> <file with pattern> <output filename>\n";
        return 1;
    }
    std::cout << "Reading settings from " << argv[1] << " and pattern from " << argv[2] << std::endl;
    Settings settings(argv[1]);
    Pattern pattern(argv[2]);
    const double complexStep = settings.getComplexStep();
    cv::Mat image(settings.imageHeight, settings.imageWidth, CV_8UC3);
    std::cout << "Rendering image..." << std::endl;
    render((Colour*)image.data, image.rows, image.cols, 0, pattern, settings.complexBegin, complexStep);
    std::cout << "Saving image in file " << argv[3] << std::endl;
    cv::imwrite(argv[3], image);
    return 0;
}
